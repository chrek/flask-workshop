import re
from datetime import datetime

from flask import Flask, render_template
from markupsafe import escape
from . import app

@app.route("/")
def home():
  return render_template("home.html")


@app.route('/about/')
def about():
  return render_template("about.html")


@app.route('/contact/')
def contact():
  return render_template("contact.html")


@app.route('/info/')
def info():
  return render_template("info.html")


@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return f'User {escape(username)}'


@app.route('/hello/')
@app.route("/hello/<name>")
def welcome(name=None):
  # now = datetime.now()
  # formatted_now = now.strftime("%A, %d %B, %Y at %X")

  # # Using regular expressions, filter the name argument
  # # to have only letters and safe characters only.
  # match_object = re.match("[a-zA-Z]+", name)

  # if match_object:
  #   clean_name = match_object.group(0)
  # else:
  #   clean_name = "Comrade"

  # content = "Hello, " + clean_name + "! It's " + formatted_now
  return render_template(
      "welcome.jinja",
      name=name,
      date=datetime.now()
  )


@app.route('/api/data')
def get_data():
  return app.send_static_file("data.json")
