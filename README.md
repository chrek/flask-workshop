# Flask-Workshop

**This is a Flask Project Workshop**

It contains a Flask app named "flask_demo_app"

The app has pages that use a common base template and access to static files using the Flask's url_for tag.

Template namespacing is also used and this allows us to point to point Flask at the right template to use.

## Files:

* views.py - This file contains the routings and the view functions
* webapp.py - This is the entry point for the application

* Templates - A template is a text file defining the structure or layout of a file (such as an HTML page), with placeholders 
  used to represent actual content. A view can dynamically create an HTML page using an HTML template, populating it with data from a model. A template can be used to define the structure of any type of file; it doesn't have to be HTML!
---


## The development server
* Set an environment variable for FLASK_APP using:
    * set FLASK_APP=webapp or export FLASK_APP=webapp
* Navigate into the flask_demo_app directory, then launch the program using:
    * python -m flask run.

## References
[code.visualstudio.com](https://code.visualstudio.com/docs/python/tutorial-flask)
[jinja](https://jinja.palletsprojects.com/en/3.0.x/templates/?highlight=stylesheets)
[w3schools](https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_contact_section)
[gist.github.com](https://gist.github.com/keeguon/2310008)
[gist.github.com](https://gist.github.com/internoma/4466866)
[ionic](https://ionic.io/ionicons/usage)
